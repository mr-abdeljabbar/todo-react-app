import React, { useState } from 'react';

const EditTodo = ({ todo, saveEdit, cancelEdit }) => {
  const [text, setText] = useState(todo.text);

  const handleSave = () => {
    if (text.trim()) {
      saveEdit(todo.id, text);
    }
  };

  return (
    <div className="edit-todo">
      <input
        type="text"
        value={text}
        onChange={(e) => setText(e.target.value)}
        placeholder="Edit todo"
      />
      <button onClick={handleSave}>Save</button>
      <button onClick={cancelEdit}>Cancel</button>
    </div>
  );
};

export default EditTodo;
