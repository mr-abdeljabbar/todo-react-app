import React from 'react';
import EditTodo from './EditTodo';

const TodoItem = ({ todo, toggleTodo, deleteTodo, startEditingTodo, isEditing, saveEdit, cancelEdit }) => {
  return (
    <li>
      {isEditing ? (
        <EditTodo todo={todo} saveEdit={saveEdit} cancelEdit={cancelEdit} />
      ) : (
        <>
          <span
            className={todo.completed ? 'completed' : ''}
            onClick={() => toggleTodo(todo.id)}
          >
            {todo.text}
          </span>
          <button onClick={() => startEditingTodo(todo.id)}><i class="fa-solid fa-pen-to-square"></i></button>
          <button onClick={() => deleteTodo(todo.id)}><i class="fa-solid fa-trash-can"></i></button>
        </>
      )}
    </li>
  );
};

export default TodoItem;
